command = "openmsx";
Add_CLP(command);
Add_CLP("-machine");



var game_script = "MSX2PLUS_50HZ.TCL";

if (Game.isExtra) {
	//Extra game
	game_script = Extra.getFileToRun();
} else {
	//Main game
	if (Value("SCRIPT").contains("*")) {
	  game_script = Value("SCRIPT");
	}	
}


if (game_script.contains("MSX1_50HZ.TCL")) {
	Add_CLP("Philips_VG_8020");	//add machine
}

if (game_script.contains("MSX2PLUS_50HZ.TCL")) {
	Add_CLP("Sony_HB-F1XDJ");	//add machine
}


Add_CLP("-script");
Add_CLP(game_script);


Dump();

if (itemType.contains("rom")) {

	Add_CLP("-carta");
	Add_CLP(itemPathAndFile);

	Run_Emulator();

} else {
	Show_Message(NOT_SUPPORTED + "\n\nSupported types: .rom");
}



